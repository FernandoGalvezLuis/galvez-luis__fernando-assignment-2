import React, {Fragment, useState, useRef, useEffect} from "react";
import { v4 as uuidv4 } from "uuid";
import { ToDoList } from  "./components/ToDoList.js";

const KEY = "todoApp.todos";


export function App() {
    const [todos, setTodos] = useState([
        { id: 1, task: "Set Tasks:", completed: false },
    ]);

    const todoTaskRef = useRef();

    useEffect(()=>{
        const storedTodos = JSON.parse(localStorage.getItem(KEY));
    if (storedTodos) {
        setTodos(storedTodos)
    }
    },[])

    useEffect(()=>{
        localStorage.setItem(KEY, JSON.stringify(todos))
    }, [todos])

    const toggleTodo = (id) => {
        const newTodos = [...todos];
        const todo = newTodos.find((todo)=> todo.id===id);
        todo.completed = !todo.completed;
        setTodos(newTodos);
    }

    const handleToDoAdd = () =>{
        const task = todoTaskRef.current.value;
        if(task === "") return alert('You cannot add an empty task')  ;

        else {setTodos((prevTodos)=>{
            return [...prevTodos, { id: uuidv4(), task, complete: false }];
        });}

        todoTaskRef.current.value = null;
    };

    const handleClearAll = () => {
        const newTodos = todos.filter((todo)=> !todo.completed)
        setTodos(newTodos)
    }
    
    return (
    <Fragment>
        <ToDoList todos={todos} toggleTodo={toggleTodo}/>
        <input ref={todoTaskRef} type="text" placeholder="new TASK"/>
        <button onClick={handleToDoAdd}>Add</button>
        <button onClick={handleClearAll}>Delete all completed</button>
        <div>{todos.filter( (todo) =>  !todo.completed   ).length}
         task/s left to finish</div>
    </Fragment>
    
    )
}