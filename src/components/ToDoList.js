import React from "react";
import { ToDoItem } from "./ToDoItem.js";

export function ToDoList({ todos, toggleTodo }) {
    return (
        <ul>
            {todos.map((todo) => (
            <ToDoItem key={todo.id} todo={todo} toggleTodo={toggleTodo} />
            ))}

        </ul>
    )
}